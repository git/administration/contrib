#!/petroglyph/perl/bin/perl
# Copyright 2023 Corwin Brust <corwin@bru.st>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#
# General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.
#
# Syndicate commit meesages sent via UDP from git post-receive hooks
# to to the server.js as per https://github.com/gyng/comicchat
#
# We get five items, space separated:
#   PROJECT CONTEXT REVISION AUTHOR MESSAGE
#
# PROJECT:  the repository name
# CONTEXT:  the branch/tag
# REVISION: the new commit hash (given as $newrev)
# AUTHOR:   the local name of the author (not the commiter)
# MESSAGE:  the first line of the commit message

use strict;
use warnings qw[all];
use feature qw[say];

use IO::Async::Loop;
use IO::Async::Socket;
use Net::Async::WebSocket::Client;

use JSON::MaybeXS;

use LWP;
use URI::Escape;

use File::Basename qw[basename dirname];

## vars

my ( $PROGRAM ) = basename $0, qw[.pl];

my $VERSION = '0.0.50';

my $DEBUG = 1; # any truthy value for more output, sent to STDOUT

my ( $default_output_path ) = ( dirname $0 );

# access controls

# accept any name or IP that exists, when empty accept any
my %accept_hosts = ();

# reject from any name or IP that exists
my %reject_hosts = ();

# keys are values to accept for project, when empty accept any
my %projects = map { $_ => 1 } qw[emacs erc dungeon test-proj cc-test-proj];

## start

# accept a new output path from command line
my ( $output_path ) = @ARGV ? (@ARGV) :
  $default_output_path . '/log';

# output path must exist
unless (-e $output_path && -r _) {
  die qq(Missing or unreadable output path "$output_path"\n).
      qq(Usage:  $0 [ /path/to/jsonl ]\n);
}

# cache existance of reject and accept lists
my $no_accept_list = %accept_hosts < 1;
my $no_acls_exist = $no_accept_list && %reject_hosts < 1;
my $accept_any_project = %projects < 1;

my $loop = IO::Async::Loop->new;

my $port = 17970;

my $socket = IO::Async::Socket->new(
   on_recv => sub {
      my ( $self, $dgram, $addr ) = @_;
      # ZZZ: fail2ban?

      # parse the datagram
      chomp($dgram);
      $DEBUG and warn 'RECV:', $dgram, "\n";

      # source ADDR check
      unless ($no_acls_exist or not exists $reject_hosts{$addr}
	      && ($no_acls_exist or exists $accept_hosts{$addr})) {
	$DEBUG and warn 'RJCT:',$addr,qq[\Q$dgram\E],"\n";
	return;
      }

      my ( $project, $context, $revision, $author, $logline ) =
	split /[\s]+/ms, $dgram, 5;

      # content validations
      unless ($project and $context and $revision and $author and $logline
	      and $revision =~ /^\b[0-9a-f]{5,40}$/
	      and ($accept_any_project
		   or exists $projects{$project})) {
	$DEBUG and warn 'RJCT:',$addr,qq[\Q$dgram\E],"\n";
	return;
      }

      my $jsonl_file = sprintf( '%s/%s.jsonl', $output_path, $project );
      open my $fh, '>>', $jsonl_file
	or do {
	  warn qq[ERR opening [[$jsonl_file][$@ $?]]:$dgram\n];
	  return;
	};

      # touch-ups

      # rtrim commit message
      $logline =~ s/[\s.]+$//m;

      # ltrim all leading semi-colon, star, and space like chars
      $logline =~ s/^[\s;]+//s;

      my $line  = encode_json({ project  => $project,
				context  => $context,
				revision => $revision,
				author   => $author,
				message  => $logline,
			      })
	. "\n";
      $DEBUG and warn qq(JSON:$line);

      $fh->print( $line ) or warn qq[ERR writing [[$jsonl_file][$@ $?]]:$dgram\n]
    }
);
$loop->add( $socket );

print "Starting..";

$socket->bind( service => $port, socktype => 'dgram' )
  ->then(sub{ say "listening on $port" })->get;

$loop->run;

__END__
