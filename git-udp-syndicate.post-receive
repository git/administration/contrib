#!/bin/sh
# Copyright 2023 Corwin Brust <corwin@bru.st>
# Copyright 2023 Paul Wise <pabs3@bonedaddy.net>
# Copyright 2023 Bob Proulx <rwp@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#
# General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.
#
# Syndicate commits via UDP on a git post-receive hook
#
# the receiving ends want five items, space separated:
#   PROJECT CONTEXT REVISION AUTHOR MESSAGE
#
# PROJECT:  the repository name
# CONTEXT:  the branch/tag
# REVISION: the new commit hash (given as $newrev)
# AUTHOR:   the local name of the author (not the commiter)
# MESSAGE:  the first line of the commit message
#
# CEZB: we argued a bit in original design, especially about quoting.
# Naturally, therefore, the below cannot reflected the preferences of
# all authors.  In particular, this code reflects the position that
# knowing the precisely required quoting as determined by the software
# required -in this case the rev-parse and log features of git(1)-
# is more powerful than safeguarding against any possible case where
# the computer may not do what the (original) authors intend or desire.
#
# Any error messages after pushing should be reported to:
# https://savannah.gnu.org/support/?func=additem&group=administration

## constants

# the repository
PROJECT=emacs

# remote UDP endpoint
DEST_ADDR=ws.comic.chat
DEST_PORT=17971

# a program that sends via UDP when we pipe output into it

## program

while read -r _oldrev newrev refname
do
    # use rev-parse to extract the tag/branch name
    context=$( git rev-parse --abbrev-ref $refname )

    # bring back AUTHOR first and MESSAGE using log
    author=$( git log $newrev -1 --pretty=format:'%ae' | cut -d '@' -f 1 | tr -d '\n')
    title="$( git log $newrev -1 --pretty=format:'%s' )"

    # finally, send the message with netcat
    printf '%s %s %s %s\n' "$PROJECT" "$context" "$newrev" "$author" "$title" |
        socat - udp:$DEST_ADDR:$DEST_PORT
done
