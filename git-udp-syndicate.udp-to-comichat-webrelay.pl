#!/petroglyph/perl/bin/perl
# Copyright 2023 Corwin Brust <corwin@bru.st>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#
# General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.
#
# Syndicate commit meesages sent via UDP from git post-receive hooks
# to to the server.js as per https://github.com/gyng/comicchat
#
# We get five items, space separated:
#   PROJECT CONTEXT REVISION AUTHOR MESSAGE
#
# PROJECT:  the repository name
# CONTEXT:  the branch/tag
# REVISION: the new commit hash (given as $newrev)
# AUTHOR:   the local name of the author (not the commiter)
# MESSAGE:  the first line of the commit message

use strict;
use warnings qw[all];
use feature qw[say];

use IO::Async::Loop;
use IO::Async::Socket;
use Net::Async::WebSocket::Client;

use JSON::MaybeXS;

use LWP;
use URI::Escape;

my %rooms; # keys are rooms, existance means joined

#use File::Basename qw[basename];
#my ($PROGRAM) = basename $0, qw[.pl];
my $VERSION = '0.0.50';
my $PROGRAM = qq[cc-udp-relay];

my $remote_baseuri = q(http://ws.comic.chat:8998);
my $ua = LWP::UserAgent->new;
$ua->agent("$PROGRAM/$VERSION");

sub forward_to_comicchat {
  my ($who, $where, $what) = @_;

  my $uri = sprintf('%s/%s/%s/%s',
		    $remote_baseuri,
		    uri_escape( $who ),
		    uri_escape( $where ),
		    uri_escape( $what ),
		   );

  my $req = HTTP::Request->new( GET => $uri );
  eval { $ua->request( $req )->status_line } || $@;
}

my $loop = IO::Async::Loop->new;

my $port = 17971;

my $socket = IO::Async::Socket->new(
   on_recv => sub {
      my ( $self, $dgram, $addr ) = @_;

      # ZZZ source ADDR check?
      chomp($dgram);
      say 'RECV:', $dgram;

      my ( $project, $context, $revision, $author, $logline ) = split /[\s]+/ms, $dgram, 5;
      return unless $project and $context and $revision and $author and $logline;
      return unless $revision =~ /^\b[0-9a-f]{5,40}$/;

      # create a room name nased on the project
      my $room = '#'.$project.'-dev';

      # make revision shorter
      substr( $revision, 6 ) = "" if length $revision > 6;

      # rtrim commit message
      $logline =~ s/[\s.]+$//m;

      my $text = qq([$context\@$revision] $logline);

      my $result = forward_to_comicchat( $author, $room, $text );

      say qq($room->($text):$result);
    }
);
$loop->add( $socket );

say "Starting..";

$socket->bind( service => $port, socktype => 'dgram' )
  ->then(sub{ say "listening on $port" })->get;

$loop->run;

__END__
