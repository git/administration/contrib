#!/usr/bin/perl
# parse STDIN assuming it is a fail2ban log report address that earned
# bans from more than one rule


# This program reads a fail2ban log from STDIN and reports address
# that have been banned 10 (or the given) or more times.  The program
# accepts an argument to change the miniumum number of times an IP
# must be banned in order to be reported.
#
# Copyright 2024 Corwin Brust <corwin@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# perminantly ban anyone who got banned 10 times per current logfile
# /root/corwin/fail2ban.find-multirule-hits.pl </var/log/fail2ban.log| perl -pne 's/^\s*//;s/\s.*$//' | grep -v -F '0.0.0.' | xargs -n 1 echo shorewall drop | perl -pe 's/shorewall/shorewall6/ if /:/' | sh

use strict;
use warnings;

# optional arg is minimum count
my $min_count = shift || 10;

my %addr;

while( <> ) {
    next unless /] Ban (\S+)$/;  # fail2ban warning or smth?
    $addr{$1}++
}

my $maxlen = 0;
foreach (keys %addr) {
    my $thislen = length;
    $maxlen = $thislen if $thislen > $maxlen;
}

for my$ip (sort { $addr{$b} <=> $addr{$a} } keys %addr) {
    next unless $addr{$ip} >= $min_count;
    printf qq(%${maxlen}s %d\n), $ip, $addr{$ip}
}
