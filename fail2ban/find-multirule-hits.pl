#!/usr/bin/perl
#

# This program reads a fail2ban log from STDIN and reports address
# that have been banned by more than one rule.
#
# Copyright 2024 Corwin Brust <corwin@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Sample shell command to elevate bans found by this script:

# /root/fail2ban/find-multirule-hits.pl </var/log/fail2ban.log \
#| perl -pne 's/^\s*//;s/\s.*$//' | grep -v -F '0.0.0.' \
#| xargs -n 1 echo shorewall drop | perl -pe 's/shorewall/shorewall6/ if /:/' \
#| sh

use strict;
use warnings;

my %addr;

while( <> ) {
    next unless /\[([^]]+)] Ban (.*)/;  # fail2ban warning or smth?
    my( $rule, $ip ) = ( $1, $2 );
    next unless $ip and $rule;

    $addr{$ip} ||= {};
    $addr{$ip}->{$rule}++
}

my $maxlen = 0;
foreach (keys %addr) {
    my $thislen = length;
    $maxlen = $thislen if $thislen > $maxlen;
}

for my$ip (sort keys %addr) {
    (my @types = keys %{$addr{$ip}}) > 1 or next;
    printf qq(%${maxlen}s %s\n), $ip, join ', ', map qq($_:$addr{$ip}{$_}), sort @types;
}
